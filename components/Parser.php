<?php
/*
 * Kомпонент который будет обращаться на другие сайт и получать от них HTML страницы, 
 * собирать на этих страницах все теги с изображениями и выводить их у себя. 
 * (cUrl запросы в php)
 */


class getParserParametrs {
    
    public function __construct() {

    }
}

class Parser extends getParserParametrs {
    // Добавить чистку линка от последнего слеша
    // Добавить валидацию линка на наличие http
    
    public $url;
    public $result;
    
    public function parseImg ($url) {
        $this->url = $link = $url;
        //$page = file_get_contents($this->url);
        
        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); //Записать http ответ в переменную, а не выводить в буфер
        $page = curl_exec($curl);
        
        $images = array();
        preg_match_all('/(img|src)=("|\')[^"\'>]+/i', $page, $media);
        $data = preg_replace('/(img|src)("|\'|="|=\')(.*)/i', "$3", $media[0]);

        foreach ($data as $url) {
            $info = pathinfo($url);
            if (isset($info['extension'])) {
                if (($info['extension'] == 'jpg') ||
                        ($info['extension'] == 'jpeg') ||
                        ($info['extension'] == 'gif') ||
                        ($info['extension'] == 'png'))
                    array_push($images, $url);
            }
        }

        $final_image = [];

        foreach ($images as $key => $value) {
            if (!in_array($value, $final_image)) {
                array_push($final_image, $value);
            }
        }

        foreach ($final_image as $value) {
            $show = $link . "/" . $value;
            $this->result .= "<img src='$show' width='300px'>";
        }
        echo $this->result;
    }
}

class showParseResult extends Parser {
    public function __toString() {
        echo $this->result;
    }
}