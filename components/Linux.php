<?php

/*
 * Компонент который будет работать с операционной системой и будет уметь следующие вещи:
        + Узнать содержимое папки (предварительно указав путь)
        + Создать папку.
        + Создать структуру папок (папку а в ней еще папку в ней еще папку и т.д)
        + Скопировать папку/файл
        + Удалить папку/файл
        + Узнать права на папку/файл
        + Изменять права на папку или файл.
        + Узнать мета данные о файле (гуглим что такое метаданные)
        + Узнавать данные об операционной системе (версия и название операционной системы)
 */
class setWorkWithLinuxParametrs {
    public $user_agent;
    public $user_browser;
    public $user_os;
    public $metaOfFile;
    
    public function __construct() {
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
    }
}

class doLinuxOperation extends setWorkWithLinuxParametrs {

    /*
     * @return array
     */
    public function showDirInside ($dirpath) {
        $indir = scandir($dirpath);
        unset($indir[array_search('.',$indir)]);
        unset($indir[array_search('..',$indir)]);
        return $indir;
    }


    // $dirstruct like "logs/testd/testd2/testd3/" , $permission like 0777
    public function createDirStruct ($dirstruct,$permission) {
        if (!file_exists($dirstruct)) {
            if (!mkdir($dirstruct, $permission, true)) {
                return "Не удалось создать директории...";
            } else {
                return "Директории $dirstruct создана";
            }
        } else {
            return "Такие директории уже есть";
        }
    }
    
    public function createDir ($dirname) {
        if (!file_exists($dirname)) {
            if (!mkdir($dirname)) {
                return "Не удалось создать директорию...";
            } else {
                return "Директория $dirname создана";
            }
        } else {
            return "Такая директория уже есть";
        }
    }
    
    public function copy ($path_original,$path_destination) {
        $exec_command = "cp -r " . $path_original . " " . $path_destination;
        // Зависит от Разрешений на изменение конечного каталога
        shell_exec($exec_command);
    }
    
    public function delete ($file) {
        exec("rm -r " . $file, $output);
    }
    
    public function getPermissions ($file) {
        return substr(sprintf('%o', fileperms($file)), -4);
    }
    
    public function changePermissions ($file,$permission) {
        if (chmod($file, $permission)) {
            return "Permission changed";
        } else {
            // Срабатывает, но лучше вылавляивать через throw. Иначе выходит и сообщение и Warning
            return "You have not enough permission to change permission.";            
        }
    }

    public function getMetaOfFile ($filename) {
        if (is_file($filename)) {
        $stat = stat($filename);
        $this->metaOfFile .= "Last change time: ".date("H:i:s d.m.Y",$stat['mtime'])."<br>";
        //$this->metaOfFile .= "Last dostup: ".date("H:i:s d.m.Y",$stat['ctime'])."<br>";  Не понятно как прочитать время создания файла
        $this->metaOfFile .= "Size of file: ". round($stat['size']/1024,2) ." Kb <br>";
        return $this->metaOfFile;
        } else {
            return "This file not exist";
        }
    }
    
    public function getUserBrowser () {
        require_once 'helper/UserOS.php';
        $user = new UserOS($this->user_agent);
        $this->user_browser = $user->getBrowser();
        return $this->user_browser;
    }
    
    public function getUserOS () {
        require_once 'helper/UserOS.php';
        $user = new UserOS($this->user_agent);
        $this->user_os = $user->getOS();
        return $this->user_os;
    }
    
}