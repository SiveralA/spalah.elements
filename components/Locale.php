<?php
/*
 * Получаем пользовательскую локализацию из заголовков запроса, находим все ключевые атрибуты данной локализации
 * (отображения, даты, времени, валюты, позиционирование текста слева направо и наоборот и тд)   
 * данные по локализации находятся в расширении php-intl (провести поисковую работу и почитать). 
 * Данный компонент должен иметь функцию по выводу даты, даты и времени, времени, валюты, вывод обычного текста 
 * (с экранирование спец. символов и HTML тегов )
 */
class setLocaleParametrs {
    public $locale;         // locale from http server
    public $timezoneD;      // default timezone !from site server
    public $language;
    public $nohtmltext;
    
    public function __construct($locale = NULL) {
        (empty($locale)) ? $this->locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']) : $this->locale = $locale;
        $this->timezoneD = date_default_timezone_get();
        $this->language = Locale::getPrimaryLanguage($this->locale);
    }
}

class finishedInput extends setLocaleParametrs {
    public $outputValue;
    
    public $moneySymbol;
    
    // Return Money symbol
    public function getCurrencySymbol($currencyCode) {
        $this->moneySymbol = new \NumberFormatter($this->locale . '@currency=' . $currencyCode, \NumberFormatter::CURRENCY);
        return $this->moneySymbol->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }
    
    // Show HTML text
    public function getNoHtmlText($text) {
        $this->nohtmltext = addslashes(htmlspecialchars($text));
        return $this->nohtmltext;
    }
    


    public function __toString() {
        return $this->language."";
    }    
}