<?php

/*
Данный компонент должен уметь:
Поддерживать сохранять данные в 4x форматах (на выбор и усмотрение программиста) JSON, XML, HTML и обычный текст.
Также компонент должен иметь возможность (как способ развития) сохранять данные вместо файлов в базу данных.
Каждый день должен создавать отдельный файл где в имени будет фигурировать дана формата DD_MM_YYYY.
 */

class setLogParametrs {

    public $dirPath = "logs/";
    public $logFormat; // JSON, XML, HTML, TXT
    public $logWay;    // File, SQL
    public $logText;
    public $nowdate;
    public $nowtime;
    public $programName;

    public function __construct($logWay, $logFormat, $programName, $logText) {
        $this->logWay = $logWay;
        $this->logFormat = $logFormat;
        $this->logText = $logText;
        $this->programName = $programName;
        $this->nowdate = date("d_m_Y");
        $this->nowtime = date("H:i:s");
        $this->logFileName = $this->nowdate . "_" . $this->programName . "." . $this->logFormat;
    }
}

class writeLog extends setLogParametrs {

    public $logValue;
    public $logTofile;

    public function packLog($status = 'update') {
        if ($this->logFormat == 'txt') {
            $this->logValue = $this->nowdate . " | " . $this->nowtime . " | " . $this->logText . " \n";
        }
        if ($this->logFormat == 'json') {
            $this->logValue = json_encode(['date' => $this->nowdate, 'time' => $this->nowtime, 'log' => $this->logText], TRUE);
        }
        if ($this->logFormat == 'html') {
            $this->logValue = $this->nowdate . " | " . $this->nowtime . " | " . $this->logText . " <br>";
        }
        if ($this->logFormat == 'xml') {
            if ($status == 'new')
                $this->logValue = '<?xml version="1.0" encoding="UTF-8"?>';
            if ($status == 'new')
                $this->logValue .= '<log>';
            $this->logValue .= '<logline>';
            $this->logValue .= '<date>' . $this->nowdate . '</date>';
            $this->logValue .= '<time>' . $this->nowtime . '</time>';
            $this->logValue .= '<text>' . $this->logText . '</text>';
            $this->logValue .= '</logline>';
        }
        return $this->logValue;
    }

    private function xmlBegin() {
        $file = file_get_contents($this->dirPath . $this->logFileName);
        $file = str_replace('</log>', '', $file);
        file_put_contents($this->dirPath . $this->logFileName, $file);
    }

    private function xmlEnd() {
        $file = file_get_contents($this->dirPath . $this->logFileName);
        $file .= '</log>';
        file_put_contents($this->dirPath . $this->logFileName, $file);
    }

    public function wrileLogToFile() {
        if (is_file($this->dirPath . $this->logFileName)) {
            if ($this->logFormat == 'xml')
                self::xmlBegin();
            $file = file_get_contents($this->dirPath . $this->logFileName);
            $this->logTofile = $file . self::packLog('update');
            file_put_contents($this->dirPath . $this->logFileName, $this->logTofile);
            if ($this->logFormat == 'xml')
                self::xmlEnd();
            return "Данные в Файл " . $this->logFileName . " добавлены.";
        } else {
            $this->logTofile = self::packLog('new');
            file_put_contents($this->dirPath . $this->logFileName, $this->logTofile);
            if ($this->logFormat == 'xml')
                self::xmlEnd();
            return "Такого файла не было. Файл " . $this->logFileName . " создан.";
        }
    }

}

class saveLog extends writeLog {

    public function __toString() {
        $this->logValue = $this->wrileLogToFile();
        return $this->logValue;
    }

}
